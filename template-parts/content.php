<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Demo_Theme
 * @since Demo Theme 1.0
 */
?>
<?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
	<span class="sticky-post"><?php _e( 'Featured', 'demotheme' ); ?></span>
<?php endif; ?>

<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

<?php the_post_thumbnail(); ?>

<?php
/* translators: %s: Name of current post */
the_content(
	sprintf(
		__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'demotheme' ),
		get_the_title()
	)
);
wp_link_pages(
	array(
		'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'demotheme' ) . '</span>',
		'after'       => '</div>',
		'link_before' => '<span>',
		'link_after'  => '</span>',
		'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'demotheme' ) . ' </span>%',
		'separator'   => '<span class="screen-reader-text">, </span>',
	)
);
?>